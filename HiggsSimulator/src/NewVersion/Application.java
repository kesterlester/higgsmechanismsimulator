package NewVersion;

import javax.swing.JFrame;
import java.awt.BorderLayout;

public class Application {

	public static void main(String[] args) {
		try {
			new Application();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Application() {

		CPP cpp = new CPP();

		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(cpp.getMainPanel(), BorderLayout.CENTER);
		frame.pack();
		frame.setVisible(true);

		// Pass control to cpp thread:
		(new Thread(cpp)).start();
	}

}
