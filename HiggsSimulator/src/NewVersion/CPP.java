package NewVersion;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;

public class CPP implements Runnable /* , ActionListener */{

	ArrayList<InitialConditioner> initialConditioners = new ArrayList<InitialConditioner>();
	
	LeapfrogDemoSetter demoSetter = null;
	
	// double narrower=1;
	String[] configNames = { "default", "intermediate", "odd", "resolutionTest", "silly1",
			"silly2", "silly3", "putPacketDebug" };
	String currentConfigName = "default";
	JComboBox<String> comboBox;

	// JTextField resetField ;
	boolean restart = true;
	boolean reset = true;

	// TODO ... the default value here should be set based on what is displayed
	// to the user.
	// NONO SetsInitialConditions initialConditioner = new SetsInitialConditionsForFourWaveDemo();

	// TODO -- this should not be here!
	// NONO public Cosmetics cosmetics;

	// NONO public Data data;
	// NONO public final LagrangianParams lp;
	TurnoffableNumber dt;
	
	private LeapfrogIntegrator integrator;
	
	private JPanel wosp = new JPanel();
	private Panel_CollectionOfWaves panel = new Panel_CollectionOfWaves();

	public static TurnoffableBool threeDims; 

	/* NONO
	public void setNonDispersive() {
		// Stabilised non-dispersive effective mass:
		cosmetics.doBigZoomAboutPositiveMin = false;
		// narrower = 1;
		applyClamp = false;
		clampTime = 27;
		setDefaultMassTerms(1);
	}
   */
	
	/* NONO
	public void setDefault() {
		// Dispersive effective mass ... and possible higgs pair production
	// NONO	cosmetics.doBigZoomAboutPositiveMin = true;
	// NONO	cosmetics.bigZoomFactor = 300;
		// narrower = 10;
		setDefaultMassTerms(10);
	}
    */
	
	/* NONO
	public void setIntermediate() {
		// Dispersive effective mass ... sort of intermediate
		cosmetics.doBigZoomAboutPositiveMin = true;
		cosmetics.bigZoomFactor = 30;
		// narrower = 3;
		applyClamp = false;
		clampTime = 27;
		setDefaultMassTerms(3);
	}
*/
	
	/* NONO
	private void setDefaultMassTerms(double narrower) {
        // TODO ... this is kludge ... shouldn't be set here AT ALL
        // instead should be set in a configurator class ...

        // In each case: mSq then quarticTerm
        if (lp.propRSF == null) {
                lp.propRSF = new RealScalarFieldProperties[lp.waves];
        }
        for (int w = 0; w < lp.waves; ++w) {
                if (lp.propRSF[w] == null) {
                        lp.propRSF[w] = new RealScalarFieldProperties(0, 0);
                }
        }
        lp.propRSF[0].reset(0 * 0, 0);
        lp.propRSF[1].reset(2.2 * 2.2, 0);
        lp.propRSF[2].reset(-2 * 2 * narrower * narrower, 0.02 * 2 * 2
                        * narrower * narrower);
        lp.propRSF[3].reset(0 * 0, 0);
        // lp.resetCaches();
    }
    */

	/* NONO
	private void setDebugMassTerms() {
		// TODO ... this is kludge ... shouldn't be set here AT ALL
		// instead should be set in a configurator class ...
		// Dispersive effective mass ... sort of intermediate
		cosmetics.doBigZoomAboutPositiveMin = false;
		//cosmetics.bigZoomFactor = 1;
		// narrower = 3;
		applyClamp = false;
		clampTime = 27;
	
		
		final double mass = 1;
		final double mSq = mass*mass;
		final double quarticTerm = 0;
		
		// In each case: mSq then quarticTerm
		if (lp.propRSF == null) {
			lp.propRSF = new RealScalarFieldProperties[lp.waves];
		}
		for (int w = 0; w < lp.waves; ++w) {
			if (lp.propRSF[w] == null) {
				lp.propRSF[w] = new RealScalarFieldProperties(mSq, quarticTerm);
			}
		}
		lp.propRSF[0].reset(mSq, quarticTerm);
		
	}
*/
	
	public CPP() {

		//Utils.checkThreeDims("CPP-constructor pre silly");
		threeDims = new TurnoffableBool(false, "3D"); 
		//Utils.checkThreeDims("CPP-constructor post silly");

		// resetField = new
		// JTextField("default, intermediate, odd, silly1, silly2");
		// resetField.addActionListener(this);


		
		comboBox = new JComboBox<String>(configNames);
		// comboBox.setSelectedIndex(0);
		comboBox.setSelectedItem(currentConfigName);
		comboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				reset = true;
			}

		});

		// NONO lp = new LagrangianParams(4);
		// NONO cosmetics = new Cosmetics();

		/* NONO
		lp.lambda012 = new TurnoffableNumber(false, 0.01, "lambda012");
		lp.lambda002 = new TurnoffableNumber(false, 0.35, "lambda002");
		lp.lambda112 = new TurnoffableNumber(false, 0.35, "lambda112");
		lp.lambda0022 = new TurnoffableNumber(true, 0.1, "lambda0022");
		lp.lambda1122 = new TurnoffableNumber(false, 0.1, "lambda1122");

		lp.dt = new TurnoffableNumber(true, 0.004, "dt");
*/
		dt = new TurnoffableNumber(true, 0.004, "dt");
		
		/*
		 * OOPS Data has propRSF[wave] fields, which are the ones modified by
		 * setDefault, etc, and which are paid attention to by the integrator,
		 * but the waves themselves have propRSFs that are paid attention to by
		 * the energy calculators. The latter are not being reset, consequently
		 * energy calcs are out. Oh dear -- we need to rationalise this. I
		 * suspect that what we want to remove is the Data-level propRSF[wave]
		 * things ... or at least to make sure that they are the same ones used
		 * elsewhere ... not reset.
		 */

		/* NONO
		setDefault(); // TODO ... shouldn't need this but the program breaks
						// without it. I don't know why.
		// setDefaultMassTerms(1); // TODO ... shouldn't need this but the
		// program breaks without it. I don't know why.

		data = new Data(lp); // TODO ... shouldn't it be the Data that has an
								// initialConditioner, not CPP, which might
								// control more than one dataset, potientially?
*/		
		integrator = new LeapfrogIntegrator();
		
		makeAllPanels(currentConfigName);
		reset = true;
	}

	

	public void makeAllPanels(String configName) {
		///////////////////////
		
		
		BagPanel bp = new BagPanel();
		// NONO bp.add(lpPanel);

		// bp.add(resetField);
		bp.add(comboBox);
		JButton restartButton = new JButton("Restart");
		

		restartButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				restart = true;
			}
		});
		bp.add(restartButton);

		Utils.checkThreeDims("CPP-makeAllPanels");
	    bp.add(threeDims.getCheckBox());  // MUST PRECEDE the panel creation below as they use threeDims

		
		////////////
		
		
	
		wosp.removeAll();
		wosp.setLayout(new BorderLayout(0, 0));

		// TODO ... a setup agent should set the following up ...

		panel.removeAll();
		initialConditioners.clear();
		integrator.clear();

		if (configName == "default") {
			new FourWaveLeapfrogDemoSetter(0,10, 300).messWith(panel, integrator, initialConditioners);
		} else if (configName == "intermediate") {
			new FourWaveLeapfrogDemoSetter(0,3, 30).messWith(panel, integrator, initialConditioners);
		} else if (configName == "odd") {
		    new FourWaveLeapfrogDemoSetter(0,1).messWith(panel, integrator, initialConditioners);
		} else if (configName =="resolutionTest") {
		    new ResolutionTestLeapfrogDemoSetter().messWith(panel, integrator, initialConditioners);			
		} else if (configName == "silly1") {
		    new FourWaveLeapfrogDemoSetter(1,1).messWith(panel, integrator, initialConditioners);			
		} else if (configName == "silly2") {
		    new FourWaveLeapfrogDemoSetter(2,1).messWith(panel, integrator, initialConditioners);			
		} else if (configName == "silly3") {
		    new FourWaveLeapfrogDemoSetter(3,1).messWith(panel, integrator, initialConditioners);			
		} else if (configName == "putPacketDebug") {
		    new DebugLeapfrogDemoSetter().messWith(panel, integrator, initialConditioners);			
		}
		
		
		wosp.add(panel, BorderLayout.CENTER);

		// NONO LagrangianParamsPanel lpPanel = new LagrangianParamsPanel(lp);
	    // NONO	wosp.add(lpPanel, BorderLayout.EAST);

		wosp.add(bp, BorderLayout.NORTH);
		
		wosp.revalidate();
	}
	
	@Override
	public void run() {
		
		final int framesPerSecond = 30;
		//System.out.println("Look out --- the repainter has not been started .... \nso redraws should not happen, but they are happening!  \nWhy? It seems that the panel is constantly invalid\nand so is redrawing all the time!");
		(new Thread(new Repainter(panel,framesPerSecond,dt))).start();

		double t = 0;

		while (true) {

			if (reset) {
				reset = false;
				String newConfigName = (String) comboBox.getSelectedItem();
				if (currentConfigName.equals(newConfigName)) {
					// I used to have false below, but actually true seems
					// better after all.
					restart = true;
				} else {
					restart = true;
				}
				currentConfigName = newConfigName;

				// TODO .. shouldn't t be in integrators???
				t = 0;

				makeAllPanels(newConfigName);
				
				// SetsInitialConditions newConditioner = null;

				/* NONO
				if (currentConfigName.equals("odd")) {
					setNonDispersive();
					initialConditioner = new SetsInitialConditionsForFourWaveDemo();
				} else if (currentConfigName.equals("intermediate")) {
					setIntermediate();
					initialConditioner = new SetsInitialConditionsForFourWaveDemo();
				} else if (currentConfigName.equals("default")) {
					setDefault();
					initialConditioner = new SetsInitialConditionsForFourWaveDemo();
				} else if (currentConfigName.equals("silly1")) {
					setNonDispersive();
					cosmetics.doBigZoomAboutPositiveMin = false;
					initialConditioner = new SetsInitialConditionsForSillyFourWaveDemo(
							1);
				} else if (currentConfigName.equals("silly2")) {
					setNonDispersive();
					cosmetics.doBigZoomAboutPositiveMin = false;
					initialConditioner = new SetsInitialConditionsForSillyFourWaveDemo(
							2);
				} else if (currentConfigName.equals("silly3")) {
					setNonDispersive();
					// setIntermediate();
					cosmetics.doBigZoomAboutPositiveMin = false;
					initialConditioner = new SetsInitialConditionsForSillyFourWaveDemo(
							3);
				} else if (currentConfigName.equals("putPacketDebug")) {
					//setDefault();
					setDebugMassTerms();
					initialConditioner = new SetsInitialConditionsForPutPacketDebug();
				} 
*/
			}

			if (restart) {
				restart = false;
				Iterator<InitialConditioner> it = initialConditioners.iterator();
				while (it.hasNext()) {
					it.next().setInitialCondition();
				}
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {

				}
			}

			// data.calcEnergies();
			// System.out.println("" + t + " E " + data.eTot+ "\t1: " +
			// data.eWaves[0] + "\t2: " + data.eWaves[1] + "\t3: " +
			// data.eWaves[2] + "\tint: " + data.eInteraction);


			{
				final double DT = dt.getValueIfOn();
				if (DT != 0) {
					integrator.advance(DT);
					t += DT;
					//System.out.println("The time is "+t+" and DT is "+DT);
				} else {
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						// do nothing
					}
				}
			}

		}

	}

	public JPanel getMainPanel() {
		return wosp;
	}

}