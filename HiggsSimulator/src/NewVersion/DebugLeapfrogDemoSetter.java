package NewVersion;

import java.util.ArrayList;

import javax.swing.JPanel;

public class DebugLeapfrogDemoSetter implements LeapfrogDemoSetter {

	public DebugLeapfrogDemoSetter() {
	}
	
	
	@Override
	public void messWith(
			JPanel panel, 
			LeapfrogIntegrator integrator,
			ArrayList<InitialConditioner> initialConditioners) {
		
		final int n = 501;
		final double L = 30;

		SpaceProperties propSpace = new SpaceProperties(n, L);
	
		final double pCentral = 2.29;
		final double pSpread = 7.13;
		final double centralX = 5;
		final double normScale = 6;
		
		try {		
					
			////////////////////////////////////////////////
			
			RSFProperties propRSF_A = new RSFProperties();
			propRSF_A.mSq = 1;
			propRSF_A.quarticTerm = 0;
			RSFWaveThing rsfWaveThing_A = new RSFWaveThing(propSpace, propRSF_A);
			RSFPanelType1.OurCosmetics cosmetics_A = new RSFPanelType1.OurCosmetics();
		
			panel.add(new RSFPanelType1(rsfWaveThing_A, cosmetics_A));
			initialConditioners.add(new RSFInitialConditioner_PacketType(
					rsfWaveThing_A,
					pCentral,
					pSpread,
					centralX,
					normScale));
			integrator.add(rsfWaveThing_A);
			
			////////////////////////////////////////////////

		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
	}

}
