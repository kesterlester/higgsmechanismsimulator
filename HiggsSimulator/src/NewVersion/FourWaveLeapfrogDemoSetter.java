package NewVersion;

import java.util.ArrayList;

import javax.swing.JPanel;

public class FourWaveLeapfrogDemoSetter implements LeapfrogDemoSetter {

	int mode; // TODO .. should be an enum ?
	private final double narrower;
	private boolean doSpecialZoomAboutNonNegVev;
	private double specialZoomFactor;
	
	public FourWaveLeapfrogDemoSetter(int mode, double narrower) {
		this.mode = mode;
		this.narrower = narrower;
		this.doSpecialZoomAboutNonNegVev = false;
	}
	
	public FourWaveLeapfrogDemoSetter(int mode, double narrower, double specialZoomFactor) {
		this.mode = mode;
		this.narrower = narrower;
		this.doSpecialZoomAboutNonNegVev = true;
		this.specialZoomFactor = specialZoomFactor;
	}
	
	@Override
	public void messWith(
			JPanel panel, 
			LeapfrogIntegrator integrator,
			ArrayList<InitialConditioner> initialConditioners) {
		
		final int n = 501;
		final double L = 30;

		SpaceProperties propSpace = new SpaceProperties(n, L);
	
		final double pCentral = 4;
		final double pSpread = 1;
		final double centralX = 5;
		final double normScale = 6;
		
		try {		
					
			////////////////////////////////////////////////
			
			RSFProperties propRSF_A = new RSFProperties();
			propRSF_A.mSq = 0;
			propRSF_A.quarticTerm = 0;
			RSFWaveThing rsfWaveThing_A = new RSFWaveThing(propSpace, propRSF_A);
			RSFPanelType1.OurCosmetics cosmetics_A = new RSFPanelType1.OurCosmetics();
		
			panel.add(new RSFPanelType1(rsfWaveThing_A, cosmetics_A));
			initialConditioners.add(new RSFInitialConditioner_PacketType(
					rsfWaveThing_A,
					pCentral,
					pSpread,
					centralX,
					normScale));
			integrator.add(rsfWaveThing_A);
			
			////////////////////////////////////////////////
			
			RSFProperties propRSF_B = new RSFProperties();
			propRSF_B.mSq = 2.2*2.2;
			propRSF_B.quarticTerm = 0;
			RSFWaveThing rsfWaveThing_B = new RSFWaveThing(propSpace, propRSF_B);
			RSFPanelType1.OurCosmetics cosmetics_B = new RSFPanelType1.OurCosmetics();
		
			panel.add(new RSFPanelType1(rsfWaveThing_B, cosmetics_B));
			initialConditioners.add(new RSFInitialConditioner_PacketType(
					rsfWaveThing_B,
					pCentral,
					pSpread,
					centralX,
					normScale));
			integrator.add(rsfWaveThing_B);
			
			////////////////////////////////////////////////
			
			{
			RSFProperties propRSF_C = new RSFProperties();
			propRSF_C.mSq = -2*2*narrower*narrower;
			propRSF_C.quarticTerm = 0.02*narrower*narrower*2*2;
			RSFWaveThing rsfWaveThing_C = new RSFWaveThing(propSpace, propRSF_C);
			RSFPanelType1.OurCosmetics cosmetics_C = new RSFPanelType1.OurCosmetics();
			if (doSpecialZoomAboutNonNegVev) {
				cosmetics_C.turnOnSpecialZoomAboutPositiveMin(specialZoomFactor);
			}
			
			RSFAABBInteraction rsfAABBInteraction_AC = new RSFAABBInteraction(
					rsfWaveThing_A,
					rsfWaveThing_C,
					0.1 // lambda0011
					);
			
			panel.add(new RSFPanelType1(rsfWaveThing_C, cosmetics_C));
			if (mode==1 || mode==2 || mode==3) {
				initialConditioners.add(new RSFInitialConditioner_HiggsSillyType(
						rsfWaveThing_C,
						mode));				
			} else {
				// default
				initialConditioners.add(new RSFInitialConditioner_HiggsTypeMatchedToRSFAABBInteraction(
					rsfWaveThing_C,
					rsfWaveThing_A,
					rsfAABBInteraction_AC));
			}
			integrator.add(rsfWaveThing_C);
			integrator.add(rsfAABBInteraction_AC);
			}
            
			////////////////////////////////////////////////
			
			RSFProperties propRSF_D = new RSFProperties();
			propRSF_D.mSq = 0;
			propRSF_D.quarticTerm = 0;
			RSFWaveThing rsfWaveThing_D = new RSFWaveThing(propSpace, propRSF_A);
			RSFPanelType1.OurCosmetics cosmetics_D = new RSFPanelType1.OurCosmetics();
		
			panel.add(new RSFPanelType1(rsfWaveThing_D, cosmetics_D));
			initialConditioners.add(new RSFInitialConditioner_PacketType(
					rsfWaveThing_D,
					pCentral,
					pSpread,
					centralX,
					normScale));
			integrator.add(rsfWaveThing_D);
			
			////////////////////////////////////////////////

		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
	}

}
