package NewVersion;

public interface Integrator {
	public void advance(double dt);
	public void clear(); // wipe all knowledge of things to evolve
}
