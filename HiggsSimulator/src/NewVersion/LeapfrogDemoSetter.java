package NewVersion;

import java.util.ArrayList;

import javax.swing.JPanel;

public interface LeapfrogDemoSetter {
	void messWith(
			JPanel panel, 
			LeapfrogIntegrator integrator,
			ArrayList<InitialConditioner> initialConditioners);
}
