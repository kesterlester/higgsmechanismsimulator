package NewVersion;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class LeapfrogIntegrator implements Integrator {

	private Set<LeapfroggableWaveThing> waveThings = new HashSet<LeapfroggableWaveThing>();
	private Set<LeapfroggableInteraction> interactions = new HashSet<LeapfroggableInteraction>();

	private void advanceVBasedOnX(double dt) {
		Iterator<LeapfroggableWaveThing> itW = waveThings.iterator();
		Iterator<LeapfroggableInteraction> itI = interactions.iterator();
		while (itW.hasNext()) {
			itW.next().advanceVBasedOnX(dt);
		}
		while (itI.hasNext()) {
			itI.next().advanceVBasedOnX(dt);
		}
	}

	private void advanceXBasedOnV(double dt) {
		Iterator<LeapfroggableWaveThing> itW = waveThings.iterator();
		// Iterator<Interaction> itI = interactions.iterator();
		while (itW.hasNext()) {
			itW.next().advanceXBasedOnV(dt);
		}
		// while(itI.hasNext()) {
		// itI.next().advanceXBasedOnV(dt);
		// }
	}

	@Override
	public void advance(double dt) {
		if (dt > 0) {
			advanceVBasedOnX(dt * 0.5);
			advanceXBasedOnV(dt * 0.5);
		} else if (dt < 0) {
			advanceXBasedOnV(dt * 0.5);
			advanceVBasedOnX(dt * 0.5);
		}
	}

	public void add(LeapfroggableWaveThing waveThing) {
		waveThings.add(waveThing);
	}

	public void add(LeapfroggableInteraction interaction) {
		interactions.add(interaction);
	}

	@Override
	public void clear() {
		waveThings.clear();
		interactions.clear();
	}

}
