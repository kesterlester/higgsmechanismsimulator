package NewVersion;

public interface LeapfroggableInteraction extends Interaction {
	public void advanceVBasedOnX(double dt);
}
