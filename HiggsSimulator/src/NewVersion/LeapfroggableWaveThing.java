package NewVersion;

public interface LeapfroggableWaveThing extends WaveThing {
	public void advanceVBasedOnX(double dt);

	public void advanceXBasedOnV(double dt);
}
