package NewVersion;

import java.awt.*;

import javax.swing.*;

@SuppressWarnings("serial")
public class Panel_CollectionOfWaves extends JPanel {

	class OurCosmetics extends GlobalCosmetics {
	}

	@SuppressWarnings("unused")
	private OurCosmetics cosmetics;
	
	private void setLayout() {		
		setLayout(new GridLayout(0, 1));
		
	}
	
	// specified cosmetics
	public Panel_CollectionOfWaves(OurCosmetics cosmetics) {
		this.cosmetics = cosmetics;
		setLayout();
	}

	// default cosmetics
	public Panel_CollectionOfWaves() {
		this.cosmetics = new OurCosmetics();
		setLayout();
	}

	/*
	 * TODO .. Put synchronisation and energy calculation in here? Or override
	 * repaintNow() ?? public void paintComponent(Graphics g) {
	 * super.paintComponent(g); //data.paintOn(g, cosmetics);
	 * 
	 * }
	 */

}
