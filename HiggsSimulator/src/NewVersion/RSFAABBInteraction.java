package NewVersion;

public class RSFAABBInteraction implements LeapfroggableInteraction {

	// This interaction term is adds      0.5*A*A*B*B      to the hamiltonian.

	RSFWaveThing A;
	RSFWaveThing B;
	double lambdaAABB;
	
	RSFAABBInteraction(
			RSFWaveThing A,
			RSFWaveThing B,
			double lambdaAABB) {
		this.A = A;
		this.B = B;
		this.lambdaAABB = lambdaAABB;
	}
	
	@Override
	public void advanceVBasedOnX(double dt) {
		
		for(int i=0; i< A.propSpace.n; ++i) {
			
			A.data.v[i] -= dt * lambdaAABB
					* A.data.x[i]
					* B.data.x[i]
					* B.data.x[i];
			
			B.data.v[i] -= dt * lambdaAABB
					* B.data.x[i]
					* A.data.x[i]
					* A.data.x[i];
		
		}
	}

}
