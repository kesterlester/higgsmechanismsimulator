package NewVersion;

public class RSFInitialConditionerUtils {
	
	// TODO .. should this be somewhere else ?
		// TODO .. should it be somewhere it can be reused with different dispersion relation for real data?
		static public void addModeWithMomIndexKn(
				RSFWaveThing wave,
				final int kn, // mom index
				final int xnCen, // index of x origin
				final double norm) {

			final double piKnOnN = wave.propSpace.piOnN * kn;
			final double twoPiKnOnN = piKnOnN * 2.0;
			final double erm = Math.sin(piKnOnN) * 2.0 / wave.propSpace.delta;
			final double en = Math.sqrt(erm * erm + wave.propRSF.mSq);

			for (int xn = 0; xn < wave.propSpace.n; ++xn) {

				// final Comp psi = exp(Comp(0,-1)*(p*xPos))*norm;
				final double arg = twoPiKnOnN * (xn - xnCen);
				final double psiReal = +Math.cos(arg) * norm;
				final double psiImag = -Math.sin(arg) * norm;

				// final Comp phiDot = Comp(0,1)*en*phi;
				final double psiDotReal = -en * psiImag;
				// final double psiDotImag = +en * psiReal;

				wave.data.x[xn] += psiReal;
				wave.data.v[xn] += psiDotReal;
			}
		}

}
