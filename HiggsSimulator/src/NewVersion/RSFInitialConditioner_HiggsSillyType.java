package NewVersion;

// TODO ... should this be an inner class of RSFWaveThing ???

public class RSFInitialConditioner_HiggsSillyType implements InitialConditioner {

	private RSFWaveThing wave;
	private int mode;
	
	public RSFInitialConditioner_HiggsSillyType(
			RSFWaveThing rsfWaveThing_higgs,
			int mode) {
		this.wave = rsfWaveThing_higgs;
		this.mode = mode;
	}


	private void setZeroAndABit() {
		for (int i = 0; i < wave.propSpace.n; ++i) {
			wave.data.x[i] = 0 * 5 + 1 * 0.00001; // Manton oscillons
			wave.data.v[i] = 0;
		}
	}

	private void setFlop() {
		for (int i = 0; i < wave.propSpace.n; ++i) {
			// x[i] = ((2*i<propSpace.n) ? +1 : -1)*propRSF.nonNegVevOrZero();
			final double c = Math.cos(2. * Math.PI * i / wave.propSpace.n);
			final double d = 2 * c * c - 1;
			wave.data.x[i] = (d) * wave.propRSF.nonNegVevOrZero();
			wave.data.v[i] = 0;
		}
	}

	private void setStaticFlop() {
		final double w = wave.propRSF.nonNegVevOrZero();
		final double higgsMassOrOne = wave.propRSF.getQuadraticMassAboutVevOrZero();
		for (int i = 0; i < wave.propSpace.n; ++i) {
			final double xPos = i * wave.propSpace.delta;
			// x[i] = ((2*i<propSpace.n) ? +1 : -1)*propRSF.nonNegVevOrZero();
			// final double c = Math.sin(2.*Math.PI*i/propSpace.n);
			// final double d = 2*c*c-1;
			wave.data.x[i] = w
					* Math.tanh((xPos - 1 * wave.propSpace.L / 4.0) * higgsMassOrOne
							/ 2)
					- w
					* Math.tanh((xPos - 3 * wave.propSpace.L / 4.0) * higgsMassOrOne
							/ 2) - w;
			wave.data.v[i] = 0;
		}
	}
	
	@Override
	public void setInitialCondition() {
		if (mode==1) {
			setZeroAndABit();
		} else if (mode==2) {
			setFlop();
		} else {
			setStaticFlop();
		}
	}
	
	// TODO ... should this be somewhere else??
	static public void putPacket(
			RSFWaveThing wave,
			double centralP,
			final double pSpread,
			final double centralX,
			final double norm) {

		final int bottomIp = (int) (centralP * wave.propSpace.oneOverDp - 0.5 * wave.propSpace.n);
		final int topIp = bottomIp + wave.propSpace.n;

		final int xnCen = (int) (centralX / wave.propSpace.delta);

		for (int ip = bottomIp; ip < topIp; ++ip) {

			final double pThatMultX = wave.propSpace.dp * ip;

			final double pDistSigs = (pThatMultX - centralP) / pSpread;
			final double pDistSigsSq = pDistSigs * pDistSigs;

			final double weight = Math.exp(-pDistSigsSq * 0.5)
					/ (Math.sqrt(2. * Math.PI) * pSpread) * wave.propSpace.dp;

			RSFInitialConditionerUtils.addModeWithMomIndexKn(wave, ip, xnCen, weight * norm);
		}
	}

	

}
