package NewVersion;

// TODO ... should this be an inner class of RSFWaveThing ???

public class RSFInitialConditioner_HiggsTypeMatchedToRSFAABBInteraction implements InitialConditioner {

	private RSFWaveThing rsfWaveThing_higgs;
	private RSFWaveThing rsfWaveThing_other;
	RSFAABBInteraction rsfAABBInteraction;
	/*
	private double pCentral;
	private double pSpread;
	private double centralX;
	private double normScale;
	*/
	
	public RSFInitialConditioner_HiggsTypeMatchedToRSFAABBInteraction(
			RSFWaveThing rsfWaveThing_higgs,
			RSFWaveThing rsfWaveThing_other,
			RSFAABBInteraction rsfAABBInteraction/*,
			double pCentral,
			double pSpread,
			double centralX,
			double normScale*/
		) {
		this.rsfWaveThing_higgs = rsfWaveThing_higgs;
		this.rsfWaveThing_other = rsfWaveThing_other;
		this.rsfAABBInteraction = rsfAABBInteraction;/*
		this.pCentral = pCentral;
		this.pSpread = pSpread;
		this.centralX = centralX;
		this.normScale = normScale;*/
	}

	@Override
	public void setInitialCondition() {
		for (int i = 0; i < rsfWaveThing_higgs.propSpace.n; ++i) {
			final double psi = rsfWaveThing_other.data.x[i];
			final double psiDot = rsfWaveThing_other.data.v[i];

			final double inside = -(rsfAABBInteraction.lambdaAABB * psi * psi + rsfWaveThing_higgs.propRSF.mSq)
					/ rsfWaveThing_higgs.propRSF.quarticTerm;
			final double insideDot = -(rsfAABBInteraction.lambdaAABB * 2 * psi * psiDot)
					/ rsfWaveThing_higgs.propRSF.quarticTerm;

			final double safeInside = inside >= 0 ? inside : 0;

			rsfWaveThing_higgs.data.x[i] = Math.sqrt(safeInside);
			rsfWaveThing_higgs.data.v[i] = 0.5 * 1 / Math.sqrt(safeInside) * insideDot;
		}
	}
	
	// TODO ... should this be somewhere else??
	static public void putPacket(
			RSFWaveThing wave,
			double centralP,
			final double pSpread,
			final double centralX,
			final double norm) {

		final int bottomIp = (int) (centralP * wave.propSpace.oneOverDp - 0.5 * wave.propSpace.n);
		final int topIp = bottomIp + wave.propSpace.n;

		final int xnCen = (int) (centralX / wave.propSpace.delta);

		for (int ip = bottomIp; ip < topIp; ++ip) {

			final double pThatMultX = wave.propSpace.dp * ip;

			final double pDistSigs = (pThatMultX - centralP) / pSpread;
			final double pDistSigsSq = pDistSigs * pDistSigs;

			final double weight = Math.exp(-pDistSigsSq * 0.5)
					/ (Math.sqrt(2. * Math.PI) * pSpread) * wave.propSpace.dp;

			RSFInitialConditionerUtils.addModeWithMomIndexKn(wave, ip, xnCen, weight * norm);
		}
	}

	

}
