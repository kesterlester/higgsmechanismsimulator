package NewVersion;

// TODO ... should this be an inner class of RSFWaveThing ???

public class RSFInitialConditioner_PacketType implements InitialConditioner {

	private RSFWaveThing rsfWaveThing;
	private double pCentral;
	private double pSpread;
	private double centralX;
	private double normScale;
	
	public RSFInitialConditioner_PacketType(RSFWaveThing rsfWaveThing,
			double pCentral,
			double pSpread,
			double centralX,
			double normScale
		) {
		this.rsfWaveThing = rsfWaveThing;
		this.pCentral = pCentral;
		this.pSpread = pSpread;
		this.centralX = centralX;
		this.normScale = normScale;
	}

	@Override
	public void setInitialCondition() {
		rsfWaveThing.data.setToZero();
		
		putPacket(rsfWaveThing,
				pCentral, // 4.0, // pCentral
				pSpread, //1.0, // pSpread,
				centralX, // centralX,
				normScale // 1 * normScale // norm
				);

	}
	
	// TODO ... should this be somewhere else??
	static public void putPacket(
			RSFWaveThing wave,
			double centralP,
			final double pSpread,
			final double centralX,
			final double norm) {

		final int bottomIp = (int) (centralP * wave.propSpace.oneOverDp - 0.5 * wave.propSpace.n);
		final int topIp = bottomIp + wave.propSpace.n;

		final int xnCen = (int) (centralX / wave.propSpace.delta);

		for (int ip = bottomIp; ip < topIp; ++ip) {

			final double pThatMultX = wave.propSpace.dp * ip;

			final double pDistSigs = (pThatMultX - centralP) / pSpread;
			final double pDistSigsSq = pDistSigs * pDistSigs;

			final double weight = Math.exp(-pDistSigsSq * 0.5)
					/ (Math.sqrt(2. * Math.PI) * pSpread) * wave.propSpace.dp;

			RSFInitialConditionerUtils.addModeWithMomIndexKn(wave, ip, xnCen, weight * norm);
		}
	}

	

}
