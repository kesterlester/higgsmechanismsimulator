package NewVersion;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

// This does a Side or a 3D panel, depending on how constructed.

@SuppressWarnings("serial")
public class RSFPanelType1 extends JPanel {

	private RSFWaveThing rsfWaveThing;
	private OurCosmetics cosmetics;

	private String currentTitle = null;

	static public class OurCosmetics extends GlobalCosmetics {
		public double basicZoomFactor = 1;
		public double specialZoomFactor;
		public boolean doSpecialZoomAboutPositiveMin = false;
		
		public void turnOnSpecialZoomAboutPositiveMin(double factor) {
			doSpecialZoomAboutPositiveMin = true;
			specialZoomFactor = factor;
		}

		public void turnOffSpecialZoomAboutPositiveMin() {
			doSpecialZoomAboutPositiveMin = false;
		}
	}

	public RSFPanelType1(RSFWaveThing rsfWaveThing, OurCosmetics cosmetics)
			throws Exception {
		if (rsfWaveThing == null) {
			throw new Exception(
					"rsfWaveThing null in RSFPanelType1 constructor.");
		}
		if (cosmetics == null) {
			throw new Exception("cosmetics null in RSFPanelType1 constructor.");
		}
		this.rsfWaveThing = rsfWaveThing;
		this.cosmetics = cosmetics;

		// TODO ... the "20s" and "120s" below should be in cosmetics!
		setBackground(Color.WHITE);
		Dimension min = new Dimension(rsfWaveThing.propSpace.n + 20 * 2, 120);
		setPreferredSize(min);
	}

	public void setMyBorder(String title) {
		currentTitle = title;
		setBorder(new TitledBorder(new LineBorder(new Color(192, 192, 192), 2,
				true), title, TitledBorder.LEADING, TitledBorder.TOP, null,
				null));
		// setBorder(new LineBorder(new Color(192, 192, 192), 2, true));
	}

	public String getMyTitle() {
		final int sigFigs = 3;
		return "mSq="
				+ Utils.doubleToStringWithSigFigs(rsfWaveThing.propRSF.mSq,
						sigFigs)

				+ ", lam="
				+ Utils.doubleToStringWithSigFigs(
						rsfWaveThing.propRSF.quarticTerm, sigFigs)

				+ ", BZF="
				+ Utils.doubleToStringWithSigFigs(cosmetics.basicZoomFactor,
						sigFigs)

				+ ", doSZ="
				+ (cosmetics.doSpecialZoomAboutPositiveMin ? ("Y, SZF=" + cosmetics.specialZoomFactor)
						: "N")

				+ ", vev=" + rsfWaveThing.propRSF.nonNegVevString(sigFigs);
	}

	public void getReadyToPaintComponent(Graphics g) {
		final String desiredTitle = getMyTitle();
		if (currentTitle == null || !(desiredTitle.equals(currentTitle))) {
			setMyBorder(desiredTitle);
		}
		super.paintComponent(g);
	}

	public void paintComponent(Graphics g) {

		getReadyToPaintComponent(g);

		int width = this.getWidth();
		int height = this.getHeight();

		final double dwidth = width;
		final double dheight = height;

		final double cosmeticScaling = 4;
		final double cosmeticScalingForE = 0.05 * 20;

		final int wid = width;

		boolean hasVev = true;
		double vev = 0;
		double vevVisualOffset = 0;
		try {
			vev = rsfWaveThing.propRSF.nonNegVev();
			if (vev != 0) {
				vevVisualOffset = cosmeticScaling * vev;
				if (!(CPP.threeDims.getValue())) {
					// 2D!

					g.setColor(Color.orange);
					g.drawLine(0,
							(int) (dheight / 2.0 - cosmeticScaling * vev), wid,
							(int) (dheight / 2.0 - cosmeticScaling * vev));
					g.drawLine(0,
							(int) (dheight / 2.0 + cosmeticScaling * vev), wid,
							(int) (dheight / 2.0 + cosmeticScaling * vev));

				}
			}
		} catch (Exception e) {
			hasVev = false;
		}

		for (int j = 0; j < rsfWaveThing.propSpace.n; ++j) {

			double xBase;
			double yBase;

		//	Utils.checkThreeDims("RSFPanelType1-paintComponent");

			if (CPP.threeDims.getValue()) {
				// 3D !
				final double angle = 2 * Math.PI * ((double) (j))
						/ ((double) (rsfWaveThing.propSpace.n));
				xBase = dwidth / 2.0 + dwidth / 4 * Math.cos(angle);
				yBase = dheight / 2.0 - dheight / 4 * Math.sin(angle);
			} else {
				// not 3D !

				xBase = j * 1 + 20;
				yBase = dheight / 2.0;
			}

			int yWave;
			if (hasVev && cosmetics.doSpecialZoomAboutPositiveMin) {
				// double vevOrZero = propRSF.nonNegVevOrZero();

				yWave = (int) (yBase - cosmeticScaling * vev - cosmeticScaling
						* cosmetics.specialZoomFactor * (rsfWaveThing.data.x[j] - vev)

				);
			} else {
				yWave = (int) (yBase - cosmeticScaling * (rsfWaveThing.data.x[j]));
			}
			final double eBin = rsfWaveThing.energyAtIndex(j);

			int yPosForE = (int) (yBase - cosmeticScalingForE * eBin);

			int ymax = yPosForE > yBase ? yPosForE : (int) yBase;
			int ymin = yPosForE < yBase ? yPosForE : (int) yBase;
			if (vev != 0 && CPP.threeDims.getValue()) {
				// 3D !
				g.setColor(Color.orange);
				g.fillRect((int) xBase, (int) (yBase + vevVisualOffset), 1, 1);
				g.fillRect((int) xBase, (int) (yBase - vevVisualOffset), 1, 1);

			}
			g.setColor(Color.cyan);
			g.fillRect((int) xBase, ymin, 1, ymax - ymin + 1);
			g.setColor(Color.black);
			g.fillRect((int) xBase, yWave, 1, 2);
		}
		// g.translate(0,-50);

	}

}
