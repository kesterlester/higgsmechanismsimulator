package NewVersion;

public class RSFWaveThing implements LeapfroggableWaveThing,
		Discrete1DEnergyDensity {

	public final SpaceProperties propSpace;
	public final RSFProperties propRSF;

	public final RealData data;

	public RSFWaveThing(SpaceProperties propSpace, RSFProperties propRSF) {
		this.propSpace = propSpace;
		this.propRSF = propRSF;
		data = new RealData(propSpace.n);
	}

	@Override
	public void advanceVBasedOnX(double dt) {
		for (int i = 0; i < data.x.length; ++i) {

			final int nex = (i + 1) % propSpace.n;
			final int pre = (i + propSpace.n - 1) % propSpace.n;

			final double dTwoPhiByDXSquared = (data.x[nex] + data.x[pre] - data.x[i] * 2.0)
					/ (propSpace.delta * propSpace.delta);

			final double phi = data.x[i];

			final double vDot =
					dTwoPhiByDXSquared
					- phi * propRSF.mSq
					- phi * phi * phi * propRSF.quarticTerm;

			data.v[i] += vDot*dt;
		}
		
	}

	@Override
	public void advanceXBasedOnV(double dt) {
		for (int i = 0; i < data.x.length; ++i) {
			data.x[i] += dt * data.v[i];
		}
	}

	@Override
	public void zeroTheWave() {
		data.setToZero();
	}

	@Override
	public double energyAtIndex(int xi) {
		final int xj = (xi + 1) % propSpace.n;
		final double dPsiByDT = (data.v[xi]);
		final double dPsiByDX = ((data.x[xj] - data.x[xi]) / propSpace.delta);
		final double psi = (data.x[xi]);

		final double ans = (0.5 * dPsiByDT * dPsiByDT + 0.5 * dPsiByDX
				* dPsiByDX + 0.5 * propRSF.mSq * psi * psi + 0.25
				* propRSF.quarticTerm * psi * psi * psi * psi - propRSF
					.isSometimesHeightOfPotMin()) * propSpace.delta;

		return ans;
	}

}
