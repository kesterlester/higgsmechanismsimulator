package NewVersion;

import java.util.Arrays;

public class RealData implements SomeKindOfXVData {
	public double[] x;
	public double[] v;

	public RealData(int n) {
		x = new double[n];
		v = new double[n];
	}

	@Override
	public void setXToZero() {
		Arrays.fill(x, 0);
	}

	@Override
	public void setVToZero() {
		Arrays.fill(v, 0);
	}

	@Override
	public void setToZero() {
		setXToZero();
		setVToZero();
	}
}
