package NewVersion;

public class Repainter implements Runnable {

	public Panel_CollectionOfWaves panel;
	final int fps;
	private final TurnoffableNumber ton;

	public Repainter(Panel_CollectionOfWaves panel, final int fps,
			TurnoffableNumber ton) {
		this.panel = panel;
		this.fps = fps;
		this.ton = ton;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		Utils.threadMessage("This indicates the start of run in the Repainter.");
		try {
			while (true) {
				Thread.sleep(1000 / fps);
				// Only repaint if the simulation is changing with time:
				if (ton != null && ton.getValueIfOn() != 0) {
					panel.repaint();
					//System.out.println("Crazy");
				}
				//System.out.println("Crozy");
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Utils.threadMessage("This indicates the end of run in the Repainter.");
	}

	public static void main_disabled(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		// System.out.println("This is the main that helps to test the Pinger");
		Utils.threadMessage("This is the main that helps to test the Repainter");

		Thread t = new Thread(new Repainter(null, 10, null));
		Utils.threadMessage("Main has created a repainter. About to start it.");
		t.start();
		Utils.threadMessage("Main has started the repainter.");

		while (t.isAlive()) {
			Utils.threadMessage("Waiting half a sec for repainter to finish.");
			t.join(500);
			Utils.threadMessage("Abandoning early.");
			t.interrupt();
		}
		Utils.threadMessage("Finished waiting.  End of main.");

	}

}
