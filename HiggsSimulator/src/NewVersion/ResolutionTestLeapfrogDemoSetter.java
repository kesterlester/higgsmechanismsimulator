package NewVersion;

import java.util.ArrayList;

import javax.swing.JPanel;

public class ResolutionTestLeapfrogDemoSetter implements LeapfrogDemoSetter {

	public ResolutionTestLeapfrogDemoSetter() {
	}
	
	private void putMode(SpaceProperties propSpace) {
		final double pCentral = 4;
		final double pSpread = 1;
		final double centralX = 5;
		final double normScale = 6;
		
		try {				
			////////////////////////////////////////////////
			
			RSFProperties propRSF_D = new RSFProperties();
			propRSF_D.mSq = 0;
			propRSF_D.quarticTerm = 0;
			RSFWaveThing rsfWaveThing_D = new RSFWaveThing(propSpace, propRSF_D);
			RSFPanelType1.OurCosmetics cosmetics_D = new RSFPanelType1.OurCosmetics();
		
			panel.add(new RSFPanelType1(rsfWaveThing_D, cosmetics_D));
			initialConditioners.add(new RSFInitialConditioner_PacketType(
					rsfWaveThing_D,
					pCentral,
					pSpread,
					centralX,
					normScale));
			integrator.add(rsfWaveThing_D);
			

		} catch (Exception e1) {
			e1.printStackTrace();
		}
			
	}
	
	private JPanel panel;
	private LeapfrogIntegrator integrator;
	private ArrayList<InitialConditioner> initialConditioners;
	
	@Override
	public void messWith(
			JPanel panel, 
			LeapfrogIntegrator integrator,
			ArrayList<InitialConditioner> initialConditioners) {
		
		this.panel = panel;
		this.integrator = integrator;
		this.initialConditioners = initialConditioners;
		
		final double L = 30;

		putMode(new SpaceProperties(251, L));
		putMode(new SpaceProperties(501, L));
		putMode(new SpaceProperties(1001, L));
		putMode(new SpaceProperties(2001, L));
	
	}

}
