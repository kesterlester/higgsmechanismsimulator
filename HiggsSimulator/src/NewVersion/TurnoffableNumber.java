package NewVersion;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JCheckBox;
import javax.swing.JTextField;

public class TurnoffableNumber implements ItemListener, ActionListener {
	private double val;
	private boolean on;
	private String label;
	private JCheckBox cb;
	private JTextField tex;

	public TurnoffableNumber(boolean initialOnState, double initialVal,
			String label) {
		this.val = initialVal;
		this.on = initialOnState;
		this.label = label;
	}

	public double getValueIfOn() {
		return on ? val : 0;
	}

	public double getValueWhetherOnOrOff() {
		return val;
	}

	/* Note this does not change the on/off state! */
	public void setVal(double val) {
		boolean alterTB = (tex != null && this.val != val);
		this.val = val;
		if (alterTB)
			matchTexToState();
	}

	public boolean isOn() {
		return on;
	}

	public boolean isOff() {
		return !on;
	}

	public void setSelected(boolean state) {
		if (cb != null && this.on != state) {
			cb.setSelected(state);
		}
		this.on = state;
	}

	public void turnOn() {
		setSelected(true);
	}

	public void turnOff() {
		setSelected(false);
	}

	public JTextField getTextField() {
		if (tex == null)
			createTextField();
		return tex;
	}

	private void createTextField() {
		tex = new JTextField();
		tex.setColumns(6);
		matchTexToState();
		tex.addActionListener(this);
	}

	private void matchTexToState() {
		getTextField().setText("" + val);
	}

	public JCheckBox getCheckBox() {
		if (cb == null)
			createCheckBox();
		return cb;
	}

	private void createCheckBox() {
		cb = new JCheckBox(label);
		cb.setSelected(this.on);
		cb.addItemListener(this);
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		on = cb.isSelected();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		boolean keepOldNumber = false;
		double newVal = 0;

		try {
			newVal = Double.parseDouble(tex.getText());
			if (Double.isNaN(newVal))
				keepOldNumber = true;
		} catch (NumberFormatException n) {
			keepOldNumber = true;
		}

		if (keepOldNumber) {
			// keep old val
			matchTexToState();
		} else {
			val = newVal;
		}
	}
}
