package WavesOnStringsPackage;

import javax.swing.JPanel;
import java.awt.FlowLayout;

public class BagPanel   extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create the panel.
	 */
	public BagPanel() {
		setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
	}

}
