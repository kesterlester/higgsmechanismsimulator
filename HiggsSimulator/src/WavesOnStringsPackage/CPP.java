package WavesOnStringsPackage;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;

public class CPP implements Runnable /* , ActionListener */{

	// double narrower=1;
	String[] configNames = { "default", "intermediate", "odd", "silly1",
			"silly2", "silly3", "putPacketDebug" };
	String currentConfigName = "default";
	JComboBox<String> comboBox;

	// JTextField resetField ;
	boolean restart = true;
	boolean reset = true;

	// TODO ... the default value here should be set based on what is displayed
	// to the user.
	SetsInitialConditions initialConditioner = new SetsInitialConditionsForFourWaveDemo();

	// TODO -- this should not be here!
	public Cosmetics cosmetics;

	public Data data;
	public final LagrangianParams lp;

	private JPanel wosp;
	private Panel_CollectionOfWaves panel;

	public static TurnoffableBool threeDims;
	
	boolean applyClamp;
	double clampTime;
	final int wider;

	public static void main_disabled(String[] args) {
		CPP cpp = new CPP();

		cpp.run();
	}

	public void setNonDispersive() {
		// Stabilised non-dispersive effective mass:
		cosmetics.doBigZoomAboutPositiveMin = false;
		// narrower = 1;
		applyClamp = false;
		clampTime = 27;
		setDefaultMassTerms(1);
	}

	public void setDefault() {
		// Dispersive effective mass ... and possible higgs pair production
		cosmetics.doBigZoomAboutPositiveMin = true;
		cosmetics.bigZoomFactor = 300;
		// narrower = 10;
		applyClamp = false;
		clampTime = 21;
		setDefaultMassTerms(10);
	}

	public void setIntermediate() {
		// Dispersive effective mass ... sort of intermediate
		cosmetics.doBigZoomAboutPositiveMin = true;
		cosmetics.bigZoomFactor = 30;
		// narrower = 3;
		applyClamp = false;
		clampTime = 27;
		setDefaultMassTerms(3);
	}

	private void setDefaultMassTerms(double narrower) {
        // TODO ... this is kludge ... shouldn't be set here AT ALL
        // instead should be set in a configurator class ...

        // In each case: mSq then quarticTerm
        if (lp.propRSF == null) {
                lp.propRSF = new RealScalarFieldProperties[lp.waves];
        }
        for (int w = 0; w < lp.waves; ++w) {
                if (lp.propRSF[w] == null) {
                        lp.propRSF[w] = new RealScalarFieldProperties(0, 0);
                }
        }
        lp.propRSF[0].reset(0 * 0, 0);
        lp.propRSF[1].reset(2.2 * 2.2, 0);
        lp.propRSF[2].reset(-2 * 2 * narrower * narrower, 0.02 * 2 * 2
                        * narrower * narrower);
        lp.propRSF[3].reset(0 * 0, 0);
        // lp.resetCaches();
    }
	
	private void setDebugMassTerms() {
		// TODO ... this is kludge ... shouldn't be set here AT ALL
		// instead should be set in a configurator class ...
		// Dispersive effective mass ... sort of intermediate
		cosmetics.doBigZoomAboutPositiveMin = false;
		//cosmetics.bigZoomFactor = 1;
		// narrower = 3;
		applyClamp = false;
		clampTime = 27;
	
		
		final double mass = 1;
		final double mSq = mass*mass;
		final double quarticTerm = 0;
		
		// In each case: mSq then quarticTerm
		if (lp.propRSF == null) {
			lp.propRSF = new RealScalarFieldProperties[lp.waves];
		}
		for (int w = 0; w < lp.waves; ++w) {
			if (lp.propRSF[w] == null) {
				lp.propRSF[w] = new RealScalarFieldProperties(mSq, quarticTerm);
			}
		}
		lp.propRSF[0].reset(mSq, quarticTerm);
		
	}

	public CPP() {

		// resetField = new
		// JTextField("default, intermediate, odd, silly1, silly2");
		// resetField.addActionListener(this);

		comboBox = new JComboBox<String>(configNames);
		// comboBox.setSelectedIndex(0);
		comboBox.setSelectedItem(currentConfigName);
		comboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				reset = true;
			}

		});

		lp = new LagrangianParams(4);
		cosmetics = new Cosmetics();

		lp.lambda012 = new TurnoffableNumber(false, 0.01, "lambda012");
		lp.lambda002 = new TurnoffableNumber(false, 0.35, "lambda002");
		lp.lambda112 = new TurnoffableNumber(false, 0.35, "lambda112");
		lp.lambda0022 = new TurnoffableNumber(true, 0.1, "lambda0022");
		lp.lambda1122 = new TurnoffableNumber(false, 0.1, "lambda1122");

		lp.dt = new TurnoffableNumber(true, 0.0008, "dt");

		wider = 1;
		// OLD:
		//final int n = 501 * wider; // Was 501 .. could be 1001 not 1000
		//final double L = 30 * wider;
		final int n = 1001 * wider; // Was 501 .. could be 1001 not 1000
		final double L = 60 * wider;

		lp.propSpace = new SpaceProperties(n, L);

		lp.propSpace.display();
		
		/*
		 * OOPS Data has propRSF[wave] fields, which are the ones modified by
		 * setDefault, etc, and which are paid attention to by the integrator,
		 * but the waves themselvs have propRSFs that are paid attention to by
		 * the energy calculators. The latter are not being reset, consequently
		 * energy calcs are out. Oh dear -- we need to reationalise this. I
		 * suspect that what we want to remove is the Data-level propRSF[wave]
		 * things ... or at least to make sure that they are the same ones used
		 * elsewhere ... not reset.
		 */

		setDefault(); // TODO ... shouldn't need this but the program breaks
						// without it. I don't know why.
		// setDefaultMassTerms(1); // TODO ... shouldn't need this but the
		// program breaks without it. I don't know why.

		data = new Data(lp); // TODO ... shouldn't it be the Data that has an
								// initialConditioner, not CPP, which might
								// control more than one dataset, potientially?

		makeAllPanels();
		reset = true;
	}

	@Override
	public void run() {
		
		final int framesPerSecond = 30;
		//System.out.println("Look out --- the repainter has not been started .... \nso redraws should not happen, but they are happening!  \nWhy? It seems that the panel is constantly invalid\nand so is redrawing all the time!");
		(new Thread(new Repainter(panel,framesPerSecond,lp.dt))).start();

		double t = 0;

		boolean wiped = false;

		while (true) {

			if (reset) {
				reset = false;
				String newConfigName = (String) comboBox.getSelectedItem();
				if (currentConfigName.equals(newConfigName)) {
					// I used to have false below, but actually true seems
					// better after all.
					restart = true;
				} else {
					restart = true;
				}
				currentConfigName = newConfigName;

				t = 0;

				// SetsInitialConditions newConditioner = null;

				if (currentConfigName.equals("odd")) {
					setNonDispersive();
					initialConditioner = new SetsInitialConditionsForFourWaveDemo();
				} else if (currentConfigName.equals("intermediate")) {
					setIntermediate();
					initialConditioner = new SetsInitialConditionsForFourWaveDemo();
				} else if (currentConfigName.equals("default")) {
					setDefault();
					initialConditioner = new SetsInitialConditionsForFourWaveDemo();
				} else if (currentConfigName.equals("silly1")) {
					setNonDispersive();
					cosmetics.doBigZoomAboutPositiveMin = false;
					initialConditioner = new SetsInitialConditionsForSillyFourWaveDemo(
							1);
				} else if (currentConfigName.equals("silly2")) {
					setNonDispersive();
					cosmetics.doBigZoomAboutPositiveMin = false;
					initialConditioner = new SetsInitialConditionsForSillyFourWaveDemo(
							2);
				} else if (currentConfigName.equals("silly3")) {
					setNonDispersive();
					// setIntermediate();
					cosmetics.doBigZoomAboutPositiveMin = false;
					initialConditioner = new SetsInitialConditionsForSillyFourWaveDemo(
							3);
				} else if (currentConfigName.equals("putPacketDebug")) {
					//setDefault();
					setDebugMassTerms();
					initialConditioner = new SetsInitialConditionsForPutPacketDebug();
				} 

			}

			if (restart) {
				restart = false;
				this.initialConditioner.writeToDataGivenLP(data, lp);
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {

				}
			}

			// data.calcEnergies();
			// System.out.println("" + t + " E " + data.eTot+ "\t1: " +
			// data.eWaves[0] + "\t2: " + data.eWaves[1] + "\t3: " +
			// data.eWaves[2] + "\tint: " + data.eInteraction);

			if (applyClamp && !wiped && t > clampTime) {
				wiped = true;
				for (int i = 0; i < lp.propSpace.n / 2 / wider; ++i) {
					data.waveData[2].x[i] = lp.propRSF[2].nonNegVevOrZero();
					data.waveData[2].v[i] = 0;
				}
			}

			{
				final double dt = lp.dt.getValueIfOn();
				if (dt != 0) {
					data.moveTimestep(dt);
					t += dt;
				} else {
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						// do nothing
					}
				}
			}

		}

	}

	public JPanel getMainPanel() {
		return wosp;
	}

	public void makeAllPanels() {

		wosp = new JPanel();
		wosp.setLayout(new BorderLayout(0, 0));

		panel = new Panel_CollectionOfWaves(data, cosmetics);
		
		
		// TODO ... a setup agent should set the following up ...
		
				 
		 
		panel.insertPanelForWave(0, cosmetics);
		panel.insertPanelForWave(1, new Cosmetics());
		panel.insertPanelForWave(2, cosmetics);
		panel.insertPanelForWave(3, cosmetics);
		panel.insertPanelForInteraction(cosmetics);

		 /*
		
		panel.add(new Panel_SingleWave3D(data, 0, cosmetics));
		panel.add(new Panel_SingleWave3D(data, 1, new Cosmetics()));
		panel.add(new Panel_SingleWave3D(data, 2, cosmetics));
		panel.add(new Panel_SingleWave3D(data, 3, cosmetics));
		
		panel.add(new Panel_Interaction(data, cosmetics));
		*/
		
		wosp.add(panel, BorderLayout.CENTER);

		LagrangianParamsPanel lpPanel = new LagrangianParamsPanel(lp);

		BagPanel bp = new BagPanel();
		bp.add(lpPanel);

		// bp.add(resetField);
		bp.add(comboBox);
		JButton restartButton = new JButton("Restart");
		

		restartButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				restart = true;
			}
		});
		bp.add(restartButton);

		threeDims = new TurnoffableBool(false, "3D"); 
		bp.add(threeDims.getCheckBox());

		
		wosp.add(lpPanel, BorderLayout.EAST);

		wosp.add(bp, BorderLayout.NORTH);
	}
}
