package WavesOnStringsPackage;

import java.awt.Color;
import java.awt.Graphics;

public class Data {

	public LagrangianParams lp;
	public WaveData[] waveData;

	double eInteraction;
	double eTot;

	// double[] eWaves;
	// double[][] eWavesPerBin;

	public void putWaveTwoToLocalMinBasedOnWaveZeroAnd0022CouplingOnly() {
		for (int i = 0; i < lp.propSpace.n; ++i) {
			final double psi = waveData[0].x[i];
			final double psiDot = waveData[0].v[i];

			final double inside = -(lp.lambda0022.getValueIfOn() * psi * psi + lp.propRSF[2].mSq)
					/ lp.propRSF[2].quarticTerm;
			final double insideDot = -(lp.lambda0022.getValueIfOn() * 2 * psi * psiDot)
					/ lp.propRSF[2].quarticTerm;

			final double safeInside = inside >= 0 ? inside : 0;

			waveData[2].x[i] = Math.sqrt(safeInside);
			waveData[2].v[i] = 0.5 * 1 / Math.sqrt(safeInside) * insideDot;
		}
	}

	public void putStationaryOffset(int wave, final double height,
			final boolean funny) {
		if (!funny) {
			for (int i = 0; i < lp.propSpace.n; ++i) {
				waveData[wave].x[i] += height;
				waveData[wave].v[i] += 0;
			}
		} else {
			for (int i = 0; i < lp.propSpace.n; ++i) {
				if (i < lp.propSpace.n / 2) {
					waveData[wave].x[i] += height;
				} else {
					waveData[wave].x[i] -= height;
				}
				waveData[wave].v[i] += 0;
			}
		}
	}

	public Data(final LagrangianParams lp) {
		reset(lp);
	}

	public void reset(final LagrangianParams lp) {
		final boolean retainExistingWaveData = (waveData != null
				&& this.lp != null && lp != null && this.lp.waves == lp.waves && this.lp.propSpace.n == lp.propSpace.n);
		this.lp = lp;
		if (retainExistingWaveData) {
			// do nothing
		} else {
			waveData = new WaveData[lp.waves];
			for (int i = 0; i < lp.waves; ++i) {
				waveData[i] = new WaveData(lp.propRSF[i], lp.propSpace);
			}
		}

		/*
		 * if (eWaves==null) { eWaves = new double[lp.waves]; }
		 */

		/*
		 * if (wWa) eWavesPerBin = new double[lp.waves+1][]; for (int w=0;
		 * w<lp.waves+1; ++w) { eWavesPerBin[w] = new double[lp.propSpace.n]; }
		 */

	}

	public void setZeroAllWaveData() {
		for (int w = 0; w < lp.waves; ++w) {
			waveData[w].setZero();
		}
	}

	public void moveTimestep(final double dt) { // "dt" can be negative to go
												// backwards in time

		final double halfdt = 0.5 * dt;
		if (halfdt > 0) {
			advanceX(halfdt);
			advanceV(halfdt);
		} else if (halfdt < 0) {
			advanceV(halfdt);
			advanceX(halfdt);
		}

	}

	private void advanceV(final double dt) {
		// Knowing x, advance v using knowledge that vDot = xDotDot = F(x)
		for (int wave = 0; wave < lp.waves; ++wave) {
			for (int i = 0; i < lp.propSpace.n; ++i) {
				waveData[wave].v[i] += dt * f(wave, i);
			}
		}

	}

	private void advanceX(final double dt) {
		// Knowing v, advance x using knowledge that xDot = v
		for (int wave = 0; wave < (lp.waves); ++wave) {
			for (int i = 0; i < (lp.propSpace.n); ++i) {
				waveData[wave].x[i] += dt * waveData[wave].v[i];
			}
		}
	}

	public double f(int wave, int i) {
		// f(i) is x''_i(t) which is a function of x_1(t), x_2(t), ... , x_n(t)
		// There are "waves" waves, each discretised to have "n" locations ...

		final int wave2 = (wave + 1) % lp.waves;
		final int wave3 = (wave + 2) % lp.waves;

		final int nex = (i + 1) % lp.propSpace.n;
		final int pre = (i + lp.propSpace.n - 1) % lp.propSpace.n;

		final double dTwoPhiByDXSquared = (waveData[wave].x[nex]
				+ waveData[wave].x[pre] - waveData[wave].x[i] * 2.0)
				/ (lp.propSpace.delta * lp.propSpace.delta);

		final double phi = waveData[wave].x[i];

		final double ans = dTwoPhiByDXSquared

				- phi
				* lp.propRSF[wave].mSq

				- phi
				* phi
				* phi
				* lp.propRSF[wave].quarticTerm

				- ((wave == 0 || wave == 1 || wave == 2) ? (lp.lambda012
						.getValueIfOn() * (waveData[wave2].x[i]) * (waveData[wave3].x[i]))
						: 0)

				- (wave != 0 ? 0 :

				lp.lambda002.getValueIfOn() * 2.0 * (waveData[0].x[i])
						* (waveData[2].x[i]) + lp.lambda0022.getValueIfOn()
						* (waveData[0].x[i]) * (waveData[2].x[i])
						* (waveData[2].x[i])

				)

				- (wave != 1 ? 0 :

				lp.lambda112.getValueIfOn() * 2.0 * (waveData[1].x[i])
						* (waveData[2].x[i]) + lp.lambda1122.getValueIfOn()
						* (waveData[1].x[i]) * (waveData[2].x[i])
						* (waveData[2].x[i])

				)

				- (wave != 2 ? 0 :

				lp.lambda002.getValueIfOn() * (waveData[0].x[i])
						* (waveData[0].x[i])

						+

						lp.lambda112.getValueIfOn() * (waveData[1].x[i])
						* (waveData[1].x[i]) + lp.lambda0022.getValueIfOn()
						* (waveData[0].x[i]) * (waveData[0].x[i])
						* (waveData[2].x[i]) + lp.lambda1122.getValueIfOn()
						* (waveData[1].x[i]) * (waveData[1].x[i])
						* (waveData[2].x[i])

				);

		return ans;

	}

	public void paintInteractionEnergyOn(Graphics g, Cosmetics cosmetics,
			int width, int height) {

		// System.out.println("Wid = ")
		EnergyDensity energyDensity = getInteractionEnergyDensity();

		final double cosmeticScaling = 4;
		final double cosmeticScalingForE = 0.05 * 20;

		final int wid = width;

		final int yPosBase = height / 2;
		// g.translate(0,50);

		g.setColor(Color.green);
		g.drawLine(0, yPosBase, wid, yPosBase);

		/*
		boolean hasVev = true;
		double vev = 0;
		try {
			vev = propRSF.nonNegVev();
			if (vev != 0) {
				g.setColor(Color.orange);
				g.drawLine(0, (int) (yPosBase - cosmeticScaling * vev), wid,
						(int) (yPosBase - cosmeticScaling * vev));
				g.drawLine(0, (int) (yPosBase + cosmeticScaling * vev), wid,
						(int) (yPosBase + cosmeticScaling * vev));
			}
		} catch (Exception e) {
			hasVev = false;
		}
*/
		
		for (int j = 0; j < lp.propSpace.n; ++j) {

			int xPos = j * 1 + 20;
			/*int yPosExtension;
			if (hasVev && cosmetics.doBigZoomAboutPositiveMin) {
				// double vevOrZero = propRSF.nonNegVevOrZero();

				yPosExtension = (int) (yPosBase - cosmeticScaling * vev - cosmeticScaling
						* cosmetics.bigZoomFactor * (x[j] - vev)

				);
			} else {
				yPosExtension = (int) (yPosBase - cosmeticScaling * (x[j]));
			}*/
			final double eBin = energyDensity.atIndex(j);

			int yPosExtensionForE = (int) (yPosBase - cosmeticScalingForE
					* eBin);

			int ymax = yPosExtensionForE > yPosBase ? yPosExtensionForE
					: yPosBase;
			int ymin = yPosExtensionForE < yPosBase ? yPosExtensionForE
					: yPosBase;
			g.setColor(Color.cyan);
			g.fillRect(xPos, ymin, 1, ymax - ymin + 1);
			//g.setColor(Color.black);
			//g.fillRect(xPos, yPosExtension, 1, 2);
		}
		// g.translate(0,-50);

	}

	private EnergyDensity getInteractionEnergyDensity() {
		return new EnergyDensity() {
			@Override
			public double atIndex(int xi) {
				
				double ans = 0;
				
				
				ans +=
					 lp.lambda012.getValueIfOn()*(
							 waveData[0].x[xi]*
							 waveData[1].x[xi]*
							 waveData[2].x[xi])
							 ;
				
				ans +=
					 lp.lambda002.getValueIfOn()*(
							 waveData[0].x[xi]*
							 waveData[0].x[xi]*
							 waveData[2].x[xi]);
				
				ans +=
					 lp.lambda112.getValueIfOn()*(
							 waveData[1].x[xi]*
							 waveData[1].x[xi]*
							 waveData[2].x[xi]);
				
				ans +=
					 0.5*lp.lambda0022.getValueIfOn()*(
							 waveData[0].x[xi]*
							 waveData[0].x[xi]*
							 waveData[2].x[xi]*
							 waveData[2].x[xi]);
					 
				ans +=
					0.5*lp.lambda1122.getValueIfOn()*(
							waveData[1].x[xi]*
							waveData[1].x[xi]*
							waveData[2].x[xi]*
							waveData[2].x[xi]);
					 
				ans *= lp.propSpace.delta;
			
				return ans;
			} // end atIndex
		}; // end EnergyDensity implementation
	}

	/*
	 * public void calcEnergies() {
	 * 
	 * eTot=0.0; for (int w=0; w<lp.waves; ++w) {
	 * 
	 * EnergyDensity energyDensity = waveData[w].getEnergyDensity();
	 * eWaves[w]=0; for (int i=0; i<lp.propSpace.n; ++i) { final double ans =
	 * energyDensity.atIndex(i); eWavesPerBin[w][i]=ans; eWaves[w] += ans; }
	 * eTot += eWaves[w]; } eInteraction = 0; for (int i=0; i<lp.propSpace.n;
	 * ++i) { eInteraction += (
	 * lp.lambda012.getValueIfOn()*(waveData[0].x[i]*waveData
	 * [1].x[i]*waveData[2].x[i]) +
	 * lp.lambda002.getValueIfOn()*(waveData[0].x[i]
	 * *waveData[0].x[i]*waveData[2].x[i]) +
	 * lp.lambda112.getValueIfOn()*(waveData
	 * [1].x[i]*waveData[1].x[i]*waveData[2].x[i]) +
	 * 0.5*lp.lambda0022.getValueIfOn
	 * ()*(waveData[0].x[i]*waveData[0].x[i]*waveData[2].x[i]*waveData[2].x[i])
	 * +
	 * 0.5*lp.lambda1122.getValueIfOn()*(waveData[1].x[i]*waveData[1].x[i]*waveData
	 * [2].x[i]*waveData[2].x[i])
	 * 
	 * )*lp.propSpace.delta
	 * 
	 * ;
	 * 
	 * }
	 * 
	 * eTot += eInteraction ; }
	 */

	/*
	 * public void paintOnMoo(Graphics g, Cosmetics cosmetics) {
	 * 
	 * calcEnergies();
	 * 
	 * final double cosmeticScaling = 5; final double cosmeticScalingForE =
	 * 0.05*20; //static std::vector<double> minEPerWave(eWavesPerBin.size());
	 * //static std::vector<bool> hasNotBeenSet(eWavesPerBin.size(),true);
	 * 
	 * for ( int w=0; w<lp.waves; ++w) { int yPosBase=(int)((w+0.5)*100);
	 * 
	 * g.setColor(Color.green); g.drawLine(0,yPosBase, 600,yPosBase);
	 * 
	 * try { final double vev = lp.propRSF[w].nonNegVev(); if (vev!=0) {
	 * g.setColor(Color.orange); g.drawLine(0,
	 * (int)(yPosBase-cosmeticScaling*vev), 600,
	 * (int)(yPosBase-cosmeticScaling*vev)); g.drawLine(0,
	 * (int)(yPosBase+cosmeticScaling*vev), 600,
	 * (int)(yPosBase+cosmeticScaling*vev)); } } catch(Exception e) { } for (int
	 * j=0; j<lp.propSpace.n; ++j) {
	 * 
	 * int xPos=j*1+20; int yPosExtension; if
	 * (cosmetics.doBigZoomAboutPositiveMin && w==2) { double vevOrZero =
	 * lp.propRSF[w].nonNegVevOrZero();
	 * 
	 * yPosExtension=(int)( yPosBase-cosmeticScaling*vevOrZero -
	 * cosmeticScaling*cosmetics.bigZoomFactor*(waveData[w].x[j]-vevOrZero )
	 * 
	 * ); } else {
	 * yPosExtension=(int)(yPosBase-cosmeticScaling*(waveData[w].x[j])); } final
	 * double eBin = eWavesPerBin[w][j]; // if (eBin < minEPerWave[w] ||
	 * hasNotBeenSet[w]) { // minEPerWave[w] = eBin; // hasNotBeenSet[w] =
	 * false; // } int
	 * yPosExtensionForE=(int)(yPosBase-cosmeticScalingForE*(eWavesPerBin[w][j]
	 * ));
	 * 
	 * //std::cout << waveData[w].x[j].real() << " " << yPosBase << " " <<
	 * yPosExtension << std::endl;
	 * 
	 * 
	 * if (true && false) { g.setColor(Color.black);
	 * g.fillRect(xPos,yPosExtension,1,1); } else if (true || false) { int ymax
	 * = yPosExtensionForE>yPosBase ? yPosExtensionForE : yPosBase; int ymin =
	 * yPosExtensionForE<yPosBase ? yPosExtensionForE : yPosBase;
	 * g.setColor(Color.cyan); g.fillRect(xPos,ymin, 1,ymax-ymin+1);
	 * g.setColor(Color.black); g.fillRect(xPos,yPosExtension,1,2); } } }
	 * 
	 * 
	 * // for (double theta=0; theta<2*lesterpi; theta+=0.1) { // ezx_color_t
	 * myColour = {(sin(theta)+1.)/2.,(cos(theta)+1.)/2,0.5}; // const double r
	 * =140; // const double x1=r*cos(theta)+150; // const double
	 * y1=r*sin(theta)+150; // const double x2=x1+10; // const double y2=y1+10;
	 * // ezx_fillrect_2d(e, (int)x1,(int)y1, (int)x2, (int)y2, &myColour); //}
	 * 
	 * 
	 * }
	 */

}
