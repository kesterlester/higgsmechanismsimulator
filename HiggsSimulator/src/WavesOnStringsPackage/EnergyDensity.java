package WavesOnStringsPackage;

public interface EnergyDensity {
	double atIndex(int xi /* x index not x pos */);
}
