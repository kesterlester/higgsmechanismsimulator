package WavesOnStringsPackage;

public class LagrangianParams {

	public LagrangianParams(int waves) {
		this.waves = waves;
		this.propRSF = new RealScalarFieldProperties[waves];
	}

	public final int waves;
	public RealScalarFieldProperties[] propRSF;

	public SpaceProperties propSpace;

	public TurnoffableNumber lambda012;
	public TurnoffableNumber lambda002;
	public TurnoffableNumber lambda112;
	public TurnoffableNumber lambda0022;
	public TurnoffableNumber lambda1122;

	// TODO ... dt is not a lagrangian param ... should be in CPP
	public TurnoffableNumber dt;
	
	// private double[] cache;

	/*
	 * double isSometimesHeightOfPotMin(final int wave) { return ( (wave>=0 &&
	 * wave < waves) ? cache[wave] : 0); } public void resetCaches() { cache =
	 * new double[waves]; for (int wave=0; wave<waves; ++wave) {
	 * 
	 * double x; try { x = propRSF[wave].nonNegVev(); // std::cout << "Addubg "
	 * << 0.5*mSq_quadTerm[wave]*x*x << " and " <<
	 * 0.25*quarticTerm[wave]*x*x*x*x << std::endl; cache[wave]=
	 * 0.5*propRSF[wave].mSq*x*x + 0.25*propRSF[wave].quarticTerm*x*x*x*x; }
	 * catch (Exception e) { // no min, so don't return a useful offset.
	 * cache[wave] = 0; }
	 * 
	 * 
	 * } }
	 */

}
