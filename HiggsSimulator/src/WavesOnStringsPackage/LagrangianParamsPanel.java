package WavesOnStringsPackage;

import javax.swing.JPanel;

import java.awt.GridLayout;

//import java.awt.GridBagLayout;
//import java.awt.GridBagConstraints;
//import java.awt.Insets;

public class LagrangianParamsPanel extends JPanel {

	// private LagrangianParams lp;

	/**
	 * 
	 */
	private static final long serialVersionUID = 2456853148910332071L;

	/**
	 * Create the panel.
	 */
	public LagrangianParamsPanel(LagrangianParams lp) {

		// this.lp = lp;

		// int y;

		/*
		 * GridBagLayout gridBagLayout = new GridBagLayout();
		 * gridBagLayout.columnWidths = new int[]{0, 0, 0, 0};
		 * gridBagLayout.rowHeights = new int[]{0, 0};
		 * gridBagLayout.columnWeights = new double[]{0.0, 0.0, 1.0,
		 * Double.MIN_VALUE}; gridBagLayout.rowWeights = new double[]{0.0,
		 * Double.MIN_VALUE}; setLayout(gridBagLayout);
		 * 
		 * GridBagConstraints gbc = new GridBagConstraints(); gbc.insets = new
		 * Insets(0, 0, 0, 5);
		 * 
		 * gbc.gridy = 0; gbc.gridx = 0; add(lp.lambda002.getCheckBox(), gbc);
		 * gbc.gridx = 1; add(lp.lambda002.getTextField(), gbc);
		 * 
		 * gbc.gridy = 1; gbc.gridx = 0; add(lp.lambda112.getCheckBox(), gbc);
		 * gbc.gridx = 1; add(lp.lambda112.getTextField(), gbc);
		 * 
		 * gbc.gridy = 2; gbc.gridx = 0; add(lp.lambda0022.getCheckBox(), gbc);
		 * gbc.gridx = 1; add(lp.lambda0022.getTextField(), gbc);
		 * 
		 * gbc.gridy = 3; gbc.gridx = 0; add(lp.lambda1122.getCheckBox(), gbc);
		 * gbc.gridx = 1; add(lp.lambda1122.getTextField(), gbc);
		 * 
		 * gbc.gridy = 4; gbc.gridx = 0; add(lp.lambda012.getCheckBox(), gbc);
		 * gbc.gridx = 1; add(lp.lambda012.getTextField(), gbc);
		 * 
		 * gbc.gridy = 5; gbc.gridx = 0; add(lp.dt.getCheckBox(), gbc);
		 * gbc.gridx = 1; add(lp.dt.getTextField(), gbc);
		 */

		setLayout(new GridLayout(0, 1));
		add(lp.lambda002.getCheckBox());
		add(lp.lambda002.getTextField());

		add(lp.lambda112.getCheckBox());
		add(lp.lambda112.getTextField());

		add(lp.lambda0022.getCheckBox());
		add(lp.lambda0022.getTextField());

		add(lp.lambda1122.getCheckBox());
		add(lp.lambda1122.getTextField());

		add(lp.lambda012.getCheckBox());
		add(lp.lambda012.getTextField());

		add(lp.dt.getCheckBox());
		add(lp.dt.getTextField());
	}

}
