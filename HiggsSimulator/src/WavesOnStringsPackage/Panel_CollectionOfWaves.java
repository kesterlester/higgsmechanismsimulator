package WavesOnStringsPackage;

import java.awt.*;

import javax.swing.*;

public class Panel_CollectionOfWaves extends JPanel {

	private final Data data;
	@SuppressWarnings("unused")
	private final Cosmetics cosmeticsForPanelCollection;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Panel_CollectionOfWaves(Data data, Cosmetics cosmetics) {
		this.data = data;
		this.cosmeticsForPanelCollection = cosmetics;

		/*
		 * setLayout(new GridLayout(1, 0, 0, 0));
		 * 
		 * GridBagLayout gridBagLayout = new GridBagLayout(); //gridBagLayout.
		 * setLayout(gridBagLayout);
		 */

		// setLayout(new SpringLayout());

		setLayout(new GridLayout(0, 1));
	}

	public void insertPanelForWave(int waveNumber, Cosmetics cosmetics) {

		Panel_SingleWave w = new Panel_SingleWave(data, waveNumber, cosmetics);

		/*
		 * GridBagConstraints gbc = new GridBagConstraints(); gbc.gridy =
		 * next++; add(w, gbc);
		 */

		add(w);

		/*
		 * SpringLayout.makeGrid(this, next, 1, //rows, cols 5, 5, //initialX,
		 * initialY 5, 5);//xPad, yPad
		 */
	}

	public void insertPanelForInteraction(Cosmetics cosmetics) {
		Panel_Interaction w = new Panel_Interaction(data, cosmetics);
		add(w);
	}

	/*
	 * TODO .. Put synchronisation and energy calculation in here? Or override
	 * repaintNow() ?? public void paintComponent(Graphics g) {
	 * super.paintComponent(g); //data.paintOn(g, cosmetics);
	 * 
	 * }
	 */

}
