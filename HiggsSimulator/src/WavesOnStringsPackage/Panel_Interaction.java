package WavesOnStringsPackage;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

public class Panel_Interaction extends JPanel {

	private final Data data; 
	private String currentTitle = null;
	
	// TODO ... check comment above!
	private Cosmetics cosmetics;
	

	public Panel_Interaction(Data data, Cosmetics cosmetics) {
		this.data = data;
		this.cosmetics = cosmetics;
		/*
		 * TODO cosmetics should ALWAYS be at the per-panel level, accessible to
		 * CPP (so that CPP set it from presets if needed) but ultimately under
		 * the control of the panel. E.g. could have one zoomed and one
		 * non-zoomed view of the same panel ... and panels could carry their
		 * own buttons/controls for setting these features.
		 */

		// TODO ... the "20s" and "120s" below should be in cosmetics!
		setBackground(Color.WHITE);
		Dimension min = new Dimension(data.lp.propSpace.n + 20 * 2, 120);
		setPreferredSize(min);
	    //setMinimumSize(min);
		//setMyBorder(getMyTitle());

	}

	public void setMyBorder(String title) {
		currentTitle = title;
		setBorder(new TitledBorder(new LineBorder(new Color(192, 192, 192), 2,
				true), title, TitledBorder.LEADING, TitledBorder.TOP,
				null, null));
		// setBorder(new LineBorder(new Color(192, 192, 192), 2, true));
	}

	public String getMyTitle() {

		//final int sigFigs = 3;

		return "Interaction Energy";
				//+ ", BZF=" + cosmetics.bigZoomFactor + ", vev="
				//+ data.waveData[wave].propRSF.nonNegVevString(sigFigs);
	}

	public void paintComponent(Graphics g) {
		//setMyBorder(); // TODO .. a bit overkill .. we really only need to do
						// this when the config changes ....
		
		final String desiredTitle = getMyTitle();
		if (currentTitle==null || !(desiredTitle.equals(currentTitle))) {
			setMyBorder(desiredTitle);
		}
			
		super.paintComponent(g);
		data.paintInteractionEnergyOn(g, cosmetics, this.getWidth(),
				this.getHeight());
	}

}
