package WavesOnStringsPackage;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

public class Panel_SingleWave extends JPanel {

	protected final int wave;
	protected final Data data; // Would it be better just to hold a pointer to
								// data[wave] instead??
	private String currentTitle = null;
	
	// TODO ... check comment above!
	protected Cosmetics cosmetics;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Panel_SingleWave(Data data, int wave, Cosmetics cosmetics) {
		this.data = data;
		this.wave = wave;
		this.cosmetics = cosmetics;
		/*
		 * TODO cosmetics should ALWAYS be at the per-panel level, accessible to
		 * CPP (so that CPP set it from presets if needed) but ultimately under
		 * the control of the panel. E.g. could have one zoomed and one
		 * non-zoomed view of the same panel ... and panels could carry their
		 * own buttons/controls for setting these features.
		 */

		// TODO ... the "20s" and "120s" below should be in cosmetics!
		setBackground(Color.WHITE);
		Dimension min = new Dimension(data.lp.propSpace.n + 20 * 2, 120);
		setPreferredSize(min);
	    //setMinimumSize(min);
		//setMyBorder(getMyTitle());

	}

	public void setMyBorder(String title) {
		currentTitle = title;
		setBorder(new TitledBorder(new LineBorder(new Color(192, 192, 192), 2,
				true), title, TitledBorder.LEADING, TitledBorder.TOP,
				null, null));
		// setBorder(new LineBorder(new Color(192, 192, 192), 2, true));
	}

	public String getMyTitle() {

		final int sigFigs = 3;

		return "Wave "
				+ wave
				+ ", mSq="
				+ Utils.doubleToStringWithSigFigs(
						data.waveData[wave].propRSF.mSq, sigFigs)
				+ ", lam="
				+ Utils.doubleToStringWithSigFigs(
						data.waveData[wave].propRSF.quarticTerm, sigFigs)
				+ ", doBZ=" + (cosmetics.doBigZoomAboutPositiveMin ? ("Y, BZF=" + cosmetics.bigZoomFactor) : "N")
				 
				+ ", vev="
				+ data.waveData[wave].propRSF.nonNegVevString(sigFigs);
	}

	public void getReadyToPaintComponent(Graphics g) {
		final String desiredTitle = getMyTitle();
		if (currentTitle==null || !(desiredTitle.equals(currentTitle))) {
			setMyBorder(desiredTitle);
		}
		super.paintComponent(g);
	}
	
	public void paintComponent(Graphics g) {
		getReadyToPaintComponent(g);
		data.waveData[wave].paintOn(g, cosmetics, this.getWidth(),
				this.getHeight());
	}

}
