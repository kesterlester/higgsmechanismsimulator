package WavesOnStringsPackage;

public class RealScalarFieldProperties {
	public double mSq = 1; // Potential includes 0.5*mSq*phi^2
	public double quarticTerm = 0; // Potential includes
									// 0.25*quarticCoupling*phi^4

	public RealScalarFieldProperties(double mSq, double quarticTerm) {
		reset(mSq, quarticTerm);
	}

	public void reset(double mSq, double quarticTerm) {
		this.mSq = mSq;
		this.quarticTerm = quarticTerm;
	}

	double isSometimesHeightOfPotMin() {
		double x;
		try {
			x = nonNegVev();
			// std::cout << "Addubg " << 0.5*mSq_quadTerm[wave]*x*x << " and "
			// << 0.25*quarticTerm[wave]*x*x*x*x << std::endl;
			return 0.5 * mSq * x * x + 0.25 * quarticTerm * x * x * x * x;
		} catch (Exception e) {
			// no min, so don't return a useful offset.
			return 0;
		}
	}

	public double getQuadraticMassAboutVevOrZero() {
		final double msq = mSq;
		final double q = quarticTerm;
		if (msq > 0) {
			if (q >= 0) {
				return Math.sqrt(mSq);
			} else {
				return 0;
			}
		} else if (msq == 0) {
			return 0;
		} else {
			assert (msq < 0);
			if (q > 0) {
				return Math.sqrt(-2. * mSq);
			} else {
				return 0;
			}
		}
	}

	public double nonNegVevOrZero() {
		try {
			return nonNegVev();
		} catch (Exception e) {
			return 0;
		}
	}

	public String nonNegVevString(int sigFigs) {
		try {
			return Utils.doubleToStringWithSigFigs(nonNegVev(), sigFigs);
			// return Double.toString(vev);
		} catch (Exception e) {
			return e.getMessage();
		}
	}

	public double nonNegVev() throws Exception {
		final double msq = mSq;
		final double q = quarticTerm;
		if (msq > 0) {
			if (q >= 0) {
				return 0;
			} else {
				throw new Exception("UFB");
			}
		} else if (msq == 0) {
			if (q > 0) {
				return 0;
			} else if (q == 0) {
				throw new Exception("FLAT");
			} else {
				throw new Exception("UFB");
			}
		} else {
			assert (msq < 0);
			if (q > 0) {
				return Math.sqrt(-msq / q);
			} else {
				throw new Exception("UFB");
			}
		}
	}
}
