package WavesOnStringsPackage;

public class SetsInitialConditionsForFourWaveDemo implements
		SetsInitialConditions {

	@Override
	public void writeToDataGivenLP(Data data, LagrangianParams lp) {

		data.reset(lp);

		double centralX = 5;
		if (data.lp.waves < 4 || data.lp.propSpace.L < centralX * 2) {
			// don't do anything as don't really have enough room
		} else {

			// data.setZeroAllWaveData();

			final double normScale = 6;

			data.waveData[0].setZero();
			data.waveData[0].putPacket(4.0, // pCentral
					1.0, // pSpread,
					centralX, // centralX,
					1 * normScale // norm
					);

			data.waveData[1].setZero();
			data.waveData[1].putPacket(+4.0, // pCentral
					1.0, // pSpread,
					centralX, // centralX,
					1 * normScale // norm
					);

			// data.waveData[2].setZeroAndABit();
			// data.putStationaryOffset(2,+lp.nonNegVev(2)*1);
			data.putWaveTwoToLocalMinBasedOnWaveZeroAnd0022CouplingOnly();

			data.waveData[3].setZero();
			data.waveData[3].putPacket(+4.0, // pCentral
					1.0, // pSpread,
					centralX, // centralX,
					1 * normScale // norm
					);

		}
	}

}
