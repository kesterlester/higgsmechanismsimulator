package WavesOnStringsPackage;

public class SetsInitialConditionsForPutPacketDebug implements
		SetsInitialConditions {

	@Override
	public void writeToDataGivenLP(Data data, LagrangianParams lp) {

		data.reset(lp);

		final double normScale = 6;
		double centralX = 5;
		if (data.lp.waves < 1 || data.lp.propSpace.L < centralX * 2) {
			// don't do anything as don't really have enough room
		} else if (false) {
			data.setZeroAllWaveData();

			data.waveData[0].putPacket(4.0, // pCentral
					1.0, // pSpread,
					centralX, // centralX,
					1 * normScale // norm
					);
		} else if (false) {
			// LOOKS WRONG
			data.setZeroAllWaveData();
			
			data.waveData[0].putPacket(2.29, // pCentral
					7.13, // pSpread,
					156.09, // centralX,
					1 // norm
					);
		} else if (false) {
			// PUTTING IN SENSIBLE L RANGE
			data.setZeroAllWaveData();
			
			data.waveData[0].putPacket(2.29, // pCentral
					7.13, // pSpread,
					centralX, // 156.09, // centralX,
					1 // norm
					);
		} else if (true) {
			// IN SENSIBLE L RANGE, AND BIGGER .. falls both ways undesirably
			data.setZeroAllWaveData();
			data.waveData[0].putPacket(2.29, // pCentral
					7.13, // pSpread,
					centralX, // 156.09, // centralX,
					1 * normScale// norm
					);
		}
	}

}
