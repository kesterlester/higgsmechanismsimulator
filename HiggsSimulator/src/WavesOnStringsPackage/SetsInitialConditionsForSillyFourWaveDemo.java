package WavesOnStringsPackage;

public class SetsInitialConditionsForSillyFourWaveDemo implements
		SetsInitialConditions {

	final private int type;

	public SetsInitialConditionsForSillyFourWaveDemo(int type) {
		this.type = type;
	}

	@Override
	public void writeToDataGivenLP(Data data, LagrangianParams lp) {

		(new SetsInitialConditionsForFourWaveDemo()).writeToDataGivenLP(data,
				lp);

		if (data.lp.waves >= 3) {

			if (type == 1) {

				data.waveData[2].setZeroAndABit();
				System.out.println("setZeroAndABit");

			} else if (type == 2) {

				data.waveData[2].setFlop();
				System.out.println("setFlop");

			} else {

				data.waveData[2].setStaticFlop();
				System.out.println("setStaticFlop");

			}
		}
	}

}
