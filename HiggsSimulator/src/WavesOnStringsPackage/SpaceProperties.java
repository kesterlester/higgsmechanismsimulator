package WavesOnStringsPackage;

public class SpaceProperties {
	public int n;
	public double piOnN;
	public double L;
	public double delta;
	public double maxMom;
	public double dp;
	public double oneOverDp;

	SpaceProperties(int n, double L) {
		reset(n, L);
	}

	void reset(int n, double L) {
		this.n = n;
		this.L = L;
		this.piOnN = Math.PI / n;
		this.delta = L / n;
		this.maxMom = Math.PI * 2.0 * n / L;
		this.dp = Math.PI * 2.0 / L;
		this.oneOverDp = 1.0 / (this.dp);
	}
	
	public void display() {
		System.out.println("Max Mom is " + this.maxMom);
	}
}
