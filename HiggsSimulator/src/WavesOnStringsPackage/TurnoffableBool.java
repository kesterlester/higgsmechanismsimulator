package WavesOnStringsPackage;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JCheckBox;

public class TurnoffableBool implements ItemListener {
	private boolean val;
	private String label;
	private JCheckBox cb;
	
	public TurnoffableBool(boolean initialVal,
			String label) {
		this.val = initialVal;
		this.label = label;
	}

	public boolean getValue() {
		return val;
	}

	public void setSelected(boolean state) {
		if (cb != null && this.val != state) {
			cb.setSelected(state);
		}
		this.val = state;
	}

	public void turnOn() {
		setSelected(true);
	}

	public void turnOff() {
		setSelected(false);
	}


	public JCheckBox getCheckBox() {
		if (cb == null)
			createCheckBox();
		return cb;
	}

	private void createCheckBox() {
		cb = new JCheckBox(label);
		cb.setSelected(this.val);
		cb.addItemListener(this);
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		val = cb.isSelected();
	}


}
