package WavesOnStringsPackage;

import java.lang.String;
import java.math.BigDecimal;
import java.math.MathContext;

public class Utils {
	// Display a message, preceded by
	// the name of the current thread
	public static void threadMessage(String message) {
		String threadName = Thread.currentThread().getName();
		System.out.format("%s: %s%n", threadName, message);
	}

	public static String doubleToStringWithSigFigs(double d, int sigFigs) {
		BigDecimal bd = new BigDecimal(d);
		bd = bd.round(new MathContext(sigFigs));
		double rounded = bd.doubleValue();
		return Double.toString(rounded);
	}

}