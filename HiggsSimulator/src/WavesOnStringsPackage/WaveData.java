package WavesOnStringsPackage;

import java.awt.Color;
import java.awt.Graphics;

// mom k = 2 pi kn / L
// pos x = xn L / N

public class WaveData {

	public double[] x;
	public double[] v;
	public final RealScalarFieldProperties propRSF;
	public final SpaceProperties propSpace;

	public WaveData(final RealScalarFieldProperties propRSF,
			final SpaceProperties propSpace) {
		x = new double[propSpace.n];
		v = new double[propSpace.n];

		this.propRSF = propRSF;
		this.propSpace = propSpace;
	}

	public void setZero() {
		for (int i = 0; i < propSpace.n; ++i) {
			x[i] = 0;
			v[i] = 0;
		}
	}

	public void setZeroAndABit() {
		for (int i = 0; i < propSpace.n; ++i) {
			x[i] = 0 * 5 + 1 * 0.000001; // /10000; // Manton oscillons
			v[i] = 0;
		}
	}

	public void setFlop() {
		for (int i = 0; i < propSpace.n; ++i) {
			// x[i] = ((2*i<propSpace.n) ? +1 : -1)*propRSF.nonNegVevOrZero();
			final double c = Math.cos(2. * Math.PI * i / propSpace.n);
			final double d = 2 * c * c - 1;
			x[i] = (d) * propRSF.nonNegVevOrZero();
			v[i] = 0;
		}
	}

	public void setStaticFlop() {
		final double w = propRSF.nonNegVevOrZero();
		final double higgsMassOrOne = propRSF.getQuadraticMassAboutVevOrZero();
		for (int i = 0; i < propSpace.n; ++i) {
			final double xPos = i * propSpace.delta;
			// x[i] = ((2*i<propSpace.n) ? +1 : -1)*propRSF.nonNegVevOrZero();
			// final double c = Math.sin(2.*Math.PI*i/propSpace.n);
			// final double d = 2*c*c-1;
			x[i] = w
					* Math.tanh((xPos - 1 * propSpace.L / 4.0) * higgsMassOrOne
							/ 2)
					- w
					* Math.tanh((xPos - 3 * propSpace.L / 4.0) * higgsMassOrOne
							/ 2) - w;
			v[i] = 0;
		}
	}

	public EnergyDensity getEnergyDensity() {
		return new EnergyDensity() {
			@Override
			public double atIndex(int xi) {
				final int xj = (xi + 1) % propSpace.n;
				final double dPsiByDT = (v[xi]);
				final double dPsiByDX = ((x[xj] - x[xi]) / propSpace.delta);
				final double psi = (x[xi]);

				final double ans = (0.5 * dPsiByDT * dPsiByDT + 0.5 * dPsiByDX
						* dPsiByDX + 0.5 * propRSF.mSq * psi * psi + 0.25
						* propRSF.quarticTerm * psi * psi * psi * psi - propRSF
							.isSometimesHeightOfPotMin()) * propSpace.delta;

				return ans;
			} // end atIndex
		}; // end EnergyDensity implementation
	} // end getEnergyDensity

	public void putPacket(double centralP, final double pSpread,
			final double centralX, final double norm) {

		final int bottomIp = (int) (centralP * propSpace.oneOverDp - 0.5 * propSpace.n);
		final int topIp = bottomIp + propSpace.n;

		final int xnCen = (int) (centralX / propSpace.delta);

		for (int ip = bottomIp; ip < topIp; ++ip) {

			final double pThatMultX = propSpace.dp * ip;

			final double pDistSigs = (pThatMultX - centralP) / pSpread;
			final double pDistSigsSq = pDistSigs * pDistSigs;

			final double weight = Math.exp(-pDistSigsSq * 0.5)
					/ (Math.sqrt(2. * Math.PI) * pSpread) * propSpace.dp;

			addModeWithMomIndexKn(ip, xnCen, weight * norm);
		}
	}

	public void addModeWithMomIndexKn(final int kn, // mom index
			final int xnCen, // index of x origin
			final double norm) {

		final double piKnOnN = propSpace.piOnN * kn;
		final double twoPiKnOnN = piKnOnN * 2.0;
		final double erm = Math.sin(piKnOnN) * 2.0 / propSpace.delta;
		final double en = Math.sqrt(erm * erm + propRSF.mSq);

		for (int xn = 0; xn < propSpace.n; ++xn) {

			// final Comp psi = exp(Comp(0,-1)*(p*xPos))*norm;
			final double arg = twoPiKnOnN * (xn - xnCen);
			final double psiReal = +Math.cos(arg) * norm;
			final double psiImag = -Math.sin(arg) * norm;

			// final Comp phiDot = Comp(0,1)*en*phi;
			final double psiDotReal = -en * psiImag;
			// final double psiDotImag = +en * psiReal;

			x[xn] += psiReal;
			v[xn] += psiDotReal;
		}
	}

	/*
	public void paintOn(Graphics g, Cosmetics cosmetics, int width, int height) {

		// System.out.println("Wid = ")
		EnergyDensity energyDensity = getEnergyDensity();

		final double cosmeticScaling = 4;
		final double cosmeticScalingForE = 0.05 * 20;

		final int wid = width;

		final int yPosBase = height / 2;
		// g.translate(0,50);

		g.setColor(Color.green);
		g.drawLine(0, yPosBase, wid, yPosBase);

		boolean hasVev = true;
		double vev = 0;
		try {
			vev = propRSF.nonNegVev();
			if (vev != 0) {
				g.setColor(Color.orange);
				g.drawLine(0, (int) (yPosBase - cosmeticScaling * vev), wid,
						(int) (yPosBase - cosmeticScaling * vev));
				g.drawLine(0, (int) (yPosBase + cosmeticScaling * vev), wid,
						(int) (yPosBase + cosmeticScaling * vev));
			}
		} catch (Exception e) {
			hasVev = false;
		}

		for (int j = 0; j < propSpace.n; ++j) {

			int xPos = j * 1 + 20;
			int yPosExtension;
			if (hasVev && cosmetics.doBigZoomAboutPositiveMin) {
				// double vevOrZero = propRSF.nonNegVevOrZero();

				yPosExtension = (int) (yPosBase - cosmeticScaling * vev - cosmeticScaling
						* cosmetics.bigZoomFactor * (x[j] - vev)

				);
			} else {
				yPosExtension = (int) (yPosBase - cosmeticScaling * (x[j]));
			}
			final double eBin = energyDensity.atIndex(j);

			int yPosExtensionForE = (int) (yPosBase - cosmeticScalingForE
					* eBin);

			int ymax = yPosExtensionForE > yPosBase ? yPosExtensionForE
					: yPosBase;
			int ymin = yPosExtensionForE < yPosBase ? yPosExtensionForE
					: yPosBase;
			g.setColor(Color.cyan);
			g.fillRect(xPos, ymin, 1, ymax - ymin + 1);
			g.setColor(Color.black);
			g.fillRect(xPos, yPosExtension, 1, 2);
		}
		// g.translate(0,-50);

	}

*/
	
	public void paintOn(Graphics g, Cosmetics cosmetics, int width, int height) {
		final double dwidth = width;
		final double dheight = height;
		
		// System.out.println("Wid = ")
				EnergyDensity energyDensity = getEnergyDensity();

				final double cosmeticScaling = 4;
				final double cosmeticScalingForE = 0.05 * 20;

				final int wid = width;

				//final int yPosBase = height / 2;
				// g.translate(0,50);

				//g.setColor(Color.green);
				//g.drawLine(0, yPosBase, wid, yPosBase);

				
				
				boolean hasVev = true;
				double vev = 0;
				double vevVisualOffset = 0;
				try {
					vev = propRSF.nonNegVev();
					if (vev != 0) {
						vevVisualOffset = cosmeticScaling*vev;
						if (!(CPP.threeDims.getValue())) {
							// 2D!
							
							g.setColor(Color.orange);
						g.drawLine(0, (int) (dheight/2.0 - cosmeticScaling * vev), wid,
								(int) (dheight/2.0 - cosmeticScaling * vev));
						g.drawLine(0, (int) (dheight/2.0 + cosmeticScaling * vev), wid,
								(int) (dheight/2.0 + cosmeticScaling * vev));
								
						
						}
					}
				} catch (Exception e) {
					hasVev = false;
				}

				
				for (int j = 0; j < propSpace.n; ++j) {

						
					double xBase; 
					double yBase; 
					
					if (CPP.threeDims.getValue()) {
						// 3D !
						final double angle = 2*Math.PI*((double)(j))/((double)(propSpace.n));
						xBase = dwidth/2.0 + dwidth/4*Math.cos(angle);
						yBase = dheight/2.0 - dheight/4*Math.sin(angle);
					} else {
						// not 3D !
						
						xBase = j*1+20;
						yBase = dheight/2.0;
					}
					
					int yWave;
					if (hasVev && cosmetics.doBigZoomAboutPositiveMin) {
						// double vevOrZero = propRSF.nonNegVevOrZero();

						yWave = (int) (yBase - cosmeticScaling * vev - cosmeticScaling
								* cosmetics.bigZoomFactor * (x[j] - vev)

						);
					} else {
						yWave = (int) (yBase - cosmeticScaling * (x[j]));
					}
					final double eBin = energyDensity.atIndex(j);

					int yPosForE = (int) (yBase - cosmeticScalingForE
							* eBin);

					
					int ymax = yPosForE > yBase ? yPosForE
							: (int)yBase;
					int ymin = yPosForE < yBase ? yPosForE
							: (int)yBase;
					if (vev !=0 && CPP.threeDims.getValue()) {
						// 3D !
						g.setColor(Color.orange);
						g.fillRect(
								(int) xBase, (int) (yBase + vevVisualOffset),
								1,1);
						g.fillRect(
								(int) xBase, (int) (yBase - vevVisualOffset),
								1,1);
								
					
					}
					g.setColor(Color.cyan);
					g.fillRect((int)xBase, ymin, 1, ymax - ymin + 1);
					g.setColor(Color.black);
					g.fillRect((int)xBase, yWave, 2, 2);
				}
				// g.translate(0,-50);

	}

}
