package WavesOnStringsPackage;

import javax.swing.JApplet;
import java.awt.BorderLayout;

public class WavesOnStringsApplet extends JApplet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6582584601202949978L;

	/**
	 * Create the applet.
	 */
	public WavesOnStringsApplet() {

		CPP cpp = new CPP();

		getContentPane().add(cpp.getMainPanel(), BorderLayout.CENTER);

		// Pass control to cpp thread:
		(new Thread(cpp)).start();
	}

}
